---
layout: default
title: Glossary
permalink: /glossary
---
# Glossary

## Game Terms

{:auto_ids}
[AGC](#agc)
: Abbreviation for “Average Game Completion” Percentage

[AStats Cap](#astats-cap)
: See [The Cap](/glossary/#the-cap)

[Brokens](#brokens)
: Not correctly implemented achievements or achievements that were added before the related content is released; also meant for achievements that can’t be gotten anymore because of patches breaking them, removing the related stuff in-game or servers shutting down for [MP-cheevos](#mp).

[Cheevo](#cheevo)
: Short for achievement. Other shortened versions such as: “Achievo”, “Chievo”, “Achs”, “Achievs”, etc. are unholy terms and you won’t get into heaven if you say them.

[Co-Op](#co-op)
: Refers to [multiplayer](#mp) achievements or games, where players are working together towards a common goal.

[Depot](#depot)
: “Depoting” a game refers to the practice of installing an older patch build of a game’s code to go back to an older version of the game. This is only allowed to be done for achievement hunters in cases where a game updated and broke a previously working achievement.

[Depoting](#depoting)
: See [Depot](#depot)

[Grind](#grind)
: “Grinding a game” refers to repeating a same set of actions again and again. “Grind” achievements are achievements that require you to do the same, tedious/boring actions many times, such as “kill 10,000 enemies” or the dreaded combo of “RNG Grind” in Monster Hunter World to find all gold crowns. Games requiring a lot of grind are known as “grindy” games.

[Hundo](#hundo)
: Short for one hundred percent achievements (100%); used as a noun and verb (“Deus Ex was a good hundo” - “I just hundoed 12 games today”). Some dangerous people slightly change this slang and say “hundro” or “hundie” (wordplay of “undies” for underwear and “hundo”).

[Invalidated](#invalidated)
: When someone has a game that appears suspicious of cheating, they will have the point value for that game nullified on various achievement trackers - that game is then referred to as “invalidated”.

[Limited](#limited)
: A “limited” achievement is any achievement in a game where a choice must be made between two or more items and that decision is final for that playthrough of the game. Some games award achievements immediately at the choice, and you can reload a save to get the other – but some games require long limited branching paths.

[Missable](#missable)
: A “missable” achievement is an achievement in a game where if you miss it and progress past that section of the game, you will not be able to return without some specific save file taking you back to that part of the game. RPGs are notorious for missables.

[MP](#mp)
: Multiplayer

[Pop](#pop)
: Verb for earning an achievement - ie: “pop an achievement”.

[Spam](#spam)
: A spam game is any game with extremely overly-granular achievements or repetitive achievements, such as “jump 1 time, 2 times, 3 times, 4 times, etc.”.

[Stacking](#stacking)
: When a game has multiple achievements for the same goal under multiple difficulties, the achievements "stack" if completing the achievement at a higher difficulty also unlocks the same lower difficulty achievements or "do not stack" if you must complete each difficulty one-by-one.

[Restricted Games](#restricted-games)
: These are games that are newly released on Steam and have not yet met certain popularity thresholds to be listed as a "trusted / real" game on Steam. They do not count as a +1 for game ownership or +1 hundo, their achievements do not count on your Steam profile, and they have a maximum of 100 cheevos. [This is Valve's countermeasure to address "Fake Games"](https://steamcommunity.com/groups/steamworks/announcements/detail/3077529424431732424) in effect since June 14, 2018.

[RNG](#rng)
: Originally stood for “Random Number Generator” but it is now used to refer to all randomness or luck in games. Certain achievements can be known as “RNG” achievements if they are significantly luck-based.

[The Cap](#the-cap)
: There are two different point caps in place on AStats:
1. 10,000 maximum point cap (this was heavily debated and is still hated by many achievement hunters).
2. 10 point cap, primarily used for spam games or games that have a high percentage of achievements that can be gotten very easily/fast - AStats has an algorithm that automatically sets these types of games as 10 point-capped.

[Unlock](#unlock)
: Verb for earning an achievement - ie: “unlock an achievement”.

## Discord Terms

{:auto_ids}
[Banned](#banned)
: Someone who is no longer allowed in 100Pals; or someone who has been removed from the leaderboards due to breaking one or more of the [rules.](/rules)

[Cheese](#cheese)
: Non-cheating strategies in games that take advantage of glitches/exploits/bad AI/non-standard gameplay to make certain challenges more easy.

[Dadbucks](#dadbucks)
: 100Pals has a long history of in-jokes involving fathers paying for games or expensive DLC/etc. - “dadbucks” are currency provided to you by a father.

[Ditch](#ditch)
: Used both jokingly and seriously, this is the verb used for when someone abandons a [scheduled game session](#mp) or does not join something they were expected to.

[Greens](#greens)
: Un[verified](#verified) members of 100Pals

Huninstall
: When you immediately uninstall a game the moment after you hundo it, usually because you hated the game and never want to think of it again.

Milk
: Opposite of [cheese](#cheese) - milk is a joke term/emote used when a user does something "very legit" such as avoiding useful glitches or refusing to use allowed tools such as autoclickers, etc.

[Purples](#purples)
: [Verified](#verified)  members of 100Pals

[Same but…](#same-but)
: Long-running in-joke started by Xeinok where users disagree to something by saying “same but… [something very different from what the original person said]”

[Scout](#scout)
: “Scouting” games is a time-honored tradition on 100Pals where players take great risks with their AGC (Average Game Completion Percentage) by exploring new games to determine if the achievements are working / how long the game takes to hundo / if any major cheese or glitches exist. A “Scout” is someone who often participates in scouting games and helps with curations on the [Achievement Scout curators](https://store.steampowered.com/curator/31507748/).

[Squad](#squad)
: A group for working on Multiplayer

[Squadron](#squadron)
: The formal name for a [squad.](#squad)

[Squad Up](#squad-up)
: Calling together a [group of players](#squad) to play a [multiplayer](#mp) game - usually imediately.

[Trap](#trap)
: "Trap" is used for a game that is known to have broken or very hard achievement(s) so that players starting it might be trapped in never being able to hundo it; also sometimes used for sketchy looking games that seem like they could have brokens or impossible to get achievements; also used for games that are very likely to get more troublesome achievements added in the near future.

[Verified](#verified)
: Unfortunately, there are many ways to cheat achievements so all 100Pals members are required to maintain a public profile and get both manually/automatically checked for any history of cheating. Getting verified is the first step to being an achievement hunter in good standing.

[VOIP](#voip)
: VOIP is the acronym for “Voice Over Internet Protocol”, also known as voice chat. 

[VOIP up](#voip-up)
: When Pals play [multiplayer](#mp) games together they will very often tell each other to “voip up” and join one of the public 100Pals [voice chat channels](#voip) together to show they are ready and to play the game more effectively with voice chat.

## Various Types of 100%

{:auto_ids}
[AStats Hundo](#astats-hundo)
: AStats counts a game as completed 100% (and award 100% bonus points) if every non-broken/working cheevo that is publicly available was unlocked.

[Brokendo](#brokendo)
: Similar to an [AStats Hundo](#astats-hundo) - another term for a hundo in a game minus known broken achievements.

[Bundo](#bundo)
: A bundo is a family-shared hundo - they are referred to as “bundos” because User Bunny infamously family-shared with many many users.

[Easydo](#easydo)
: [Hundos](#hundo)  that are very easy and often quick.

[Fastdo](#fastdo)
: When person completes a game in a shorter than average time due to impressive skill or luck with [RNG](#rng) stuff; can also be used to describe short games in general.

[Firstdo](#firstdo)
: If you are the first person to [hundo](#hundo)  a game on any tracker-site or first one to re-hundo it after a content update, etc.

[Fundo](#fundo)
: A [hundo](#hundo) that was particularly good and had enjoyable achievements.

[Refundo](#refundo)
: The act of purchasing a game, quickly [hundoing](#hundo) it under the 2-hour return limit, and then requesting a refund for it ("refundoing" a game). This is a scummy, deplorable act no matter how "bad" you think the game is and it will get you [banned](#banned) from the community quickly.

[Rehundo](#rehundo)
: Getting the [hundo](#hundo) in a game again, after it got more achievements, for example due to DLCs getting added. Rehundos are very common on Steam where games are often living things receiving updates and patches through the years.

[Restrictedo](#restrictedo)
: A [hundo](#hundo) of a [restricted game](#restricted-games).

[Sacred 1mindo](#sacred-1mindo)
: A [hundo](#hundo) that is complete with only 1minute of in-game time (the minimum possible since Steam starts at 1min).

[Set](#set)
: A set is a completion of a series of games, like beating all games made by Valve or all games that take place in the Final Fantasy story world.

[Setdo](#setdo)
: Getting a [hundo](#hundo) for a full [set](#set).

[Speedrun Hundo](#speedrun-hundo)
: See [Fastdo.](#fastdo)

[Spamdo](#spamdo)
: A [hundo](#hundo) of a [spam](#spam) game.

[Theoretical Hundo](#theoretical-hundo)
: When you have completed everything in a game except for a very small number of broken/not understood achievements - often used in a joking way when someone simply has not figured out the final achievements.


## Achievement Trackers

{:auto_ids}
[Tracker](#tracker)
: An “achievement tracker” is a website/service built specifically for tracking detailed achievement statistics, often including leaderboards. Visit the [Resources](/resources) page to see more info about our recommended trackers.

List of Trackers:

### Steam

[AStats](https://astats.astats.nl/astats) - AStats.nl, the OG Steam Tracker with points based Leaderboards created by Mytharox 🇳🇱

[SH](http://steamhunters.com/) - SteamHunters.com, modern Tracker with points based Leaderboards created by Rudey 🇳🇱

[TSA](https://truesteamachievements.com/) - TrueSteamAchievements.com, Steam iteration of the TrueNetwork Trackers created by Rich Stone 🇬🇧

### Console

[TA](https://www.trueachievements.com/) - TrueAchievements, Xbox Achievements and Gamerscore Tracker with tools and guides, original TrueNetwork tracker, created by Rich Stone 🇬🇧

[TT](https://www.truetrophies.com/) - TrueTrophies, PlayStation Achievements Tracker with tools and guides, created by Rich Stone 🇬🇧

[PSNP](https://psnprofiles.com/) - PSNProfiles, PlayStation consoles Achievement Tracker with leaderboards and beautiful profiles, created by Gaming Profiles Ltd. 🇬🇧

### Multiplatform

[CME](https://completionist.me/) - completionist.me, Personal Achievement Tracker created by luchaos 🇦🇹

[EXO](https://www.exophase.com/) - Exophase, Multiplatform tracker covering consoles/PC/mobile with simplistic leaderboards/stats, created by Mike Bendel 🇺🇸

[MGS](http://metagamerscore.com/) - MetaGamerScore.com, Multi Platform Tracker with points based Leaderboards and Ranked Crews created by primal_r 🇸🇪

[RA](https://retroachievements.org/) - RetroAchievements.org, Achievements for Games on Retro Consoles with points based Leaderboards created by ScottFromDerby 🇬🇧
