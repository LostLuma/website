---
layout: social
title: Resources
permalink: /resources
---
# Resources

## Partnered Achievement Trackers

### [Completionist.me](https://completionist.me/)

![CME][icon-cme]Also known as “c.me” or “cme”, and developed solely by **luchaos** since 2014. cme is a statistics-focused website where users are offered a wide range of useful tools/graphs/charts and endless filters regarding their Steam and RetroAchievements data. While cme currently lacks leaderboards, luchaos is focused on making hunting easier and more accessible by simplifying the huge amounts of public information on our Steam profiles with cme’s beautiful, simplistic design. Completionst.me powers a real-time feed of EVERY achievement change across all of Steam’s 20,000+ games and apps in the #steam-feed channel on the [100Pals Discord](https://discord.gg/100Pals).

### [Exophase](https://www.exophase.com/)

![EXO][icon-exo]Exophase started as a gaming news site way back in 2006 but then shifted to a multi-platform achievement tracker in 2011. Created by **x3sphere**, Exophase was one of the first sites to offer customizable, multi-platform gamercards which enable users to display a snapshot of their gaming activity across other sites and forums. Exophase supports tracker and leaderboard functions for almost all game consoles and PC gaming platforms.

### [MetaGamerScore](http://metagamerscore.com/)

![MGS][icon-mgs]Multi-platforming achievement hunters will love MGS, released in 2011. It supports and combines almost every gaming platform with achievements including: Steam, XBox, PlayStation, GOG, RetroAchievements, WoW, FFXIV, and many more. Oskar Holmkratz (**primal_r**, the head developer), Linda Holmkratz, Martin Berglund and Geilson Fonte worked on making this dream come true, a place where you can combine everything you have hunted across every platform. MGS contains several interesting leaderboards, including crew leaderboards where [100Pals has been the Number 1 worldwide team](https://metagamerscore.com/most_score_crew) for years.

Don't forget to join our [100Pals MGS Crew](https://metagamerscore.com/crews/256)! The password is “trolleybus”.

### [Steam Hunters](http://steamhunters.com/)

![SH][icon-sh]Steam Hunters, developed by **Rudey**, is the most recently-created achievement tracker, having started development in 2015 with release to the public in January 2017. SH is a Steam-only tracker that uses a unique point formula for its leaderboards where a game’s total value is based on its rarest achievement. Rudey, the one and only creator of this fastly-growing tracker states that it is "Yet Another Steam Achievement Tracker", but SH is a great new contender for Steam achievement hunter leaderboards and is updated very regularly.

## Curators

### [Achievement Scouts](https://store.steampowered.com/curator/31507748/), [Achievement Scouts 2](https://store.steampowered.com/curator/33207241/) , and [Achievement Scouts: Restricted](https://store.steampowered.com/curator/34752873)

![Scouts][icon-cur-scout]Simple & direct curation of all Steam games to confirm that the achievements are working, to quantify effort/time, and to post major time-saving techniques.

### [100Pals "Good Games to 100%" Curator](https://store.steampowered.com/curator/25002676/)

![Pals][icon-cur-pals]Great games that reflect the many tastes of the 100Pals Achievement Hunting Community.

### [VR Achievement Hunters](https://store.steampowered.com/curator/29354216/)

![VR][icon-cur-vr]Testing out the achievements in VR games.

## 100Pals Projects

**[100Pals Workshop](https://discord.gg/WrSzFWs)** - Discord server with public and private channels for discussion of various 100Pals projects and events.

**[100Pals Championship](https://docs.google.com/spreadsheets/d/1TyAfwFq74jAj1zzjogFoWbIGf7JEwgwBozLCzfBiXZ0/edit)** - We hold regular events for fame and glory.

**[Favorite Achievements Mosaic](https://completionist.me/place/100Pals)** - The Pals came together and built a massive mosaic of all our favorite achievements over the years.

**[Transferrable Game Saves List](https://docs.google.com/spreadsheets/d/1tOytn7qPyB8tr8GSsaweY9A-tcnKshmy6MUUMsneqFY/edit)** - List of games that can import saves from other platforms such as mobile and PlayStation.

**[Security Tips for Hunters](/security)** - Keep your cheevos and accounts safe with these security tips!

## Tools

**[Achievement Tracker Userscripts](https://completionist.me/tools)** - Userscripts that allow you to very easily move between various achievement trackers + Steam + SteamDB.

**[CME Missing Achievements/Games Recovery Tool](https://completionist.me/steam/recover)** - Completionist.me's recovery tool for if achievement trackers and your Steam have desynchronized what achievements and games they show you.

**[Steam Hunters Recovery Tool](https://steamhunters.com/tools)** - Recovery tool specifically for Steam Hunters to attempt to properly sync your profile by finding missing games.

## 100Pals Crews & Guilds

**[CasinoRPG](https://store.steampowered.com/app/658970/CasinoRPG/)** - "100Pals" - Be at least Level 8 then ask in #casino-pals for a new slot to be added.

**[Clicker Heroes](https://store.steampowered.com/app/363970/Clicker_Heroes/)** - "TheEnclave", "100Clicks", "Caamora". Limited to 10 people each. Check in #clicker-pals for more info.

**[Dragon Lords: 3D Strategy](https://store.steampowered.com/app/661960/Dragon_Lords_3D_Strategy/)** - "100Pals" - First Build a Guild Tower and then search for 100Pals and join.

**[Elder Scrolls Online](https://store.steampowered.com/app/306130/The_Elder_Scrolls_Online/)** - "AchievementPals" (Pc) - Add @Allalinor, @DrFestus or @AmorousEyes ingame to get added.

**[Forza Horizon 3](https://www.xbox.com/en-US/games/forza-horizon-3)** - "P4L5" (XBox/Win10) - Message STGMDJ on XBox Live to get added.

**[Grand Theft Auto V](https://store.steampowered.com/app/271590/Grand_Theft_Auto_V/)** - "100Pals" - [Rockstar Games Social Club](https://socialclub.rockstargames.com/crew/100Pals)

**[Guns of Icarus](https://store.steampowered.com/app/209080/Guns_of_Icarus_Online/)** - "100p"

**[Mini Guns](https://store.steampowered.com/app/649900/Mini_Guns__Omega_Wars/)** - "100Pals" (Steam) - Invitations only. Mention YouGotHitByGunner. Limited to 20 people.

**[Modern Combat Versus](https://store.steampowered.com/app/723780/Modern_Combat_Versus/)** - "100Pals" (Steam) - Invitations only. Mention YouGotHitByGunner. Limited to 20 people. Note: At least 500 trophies needed.

**[Sniper Fury](https://store.steampowered.com/app/591740/Sniper_Fury/)** - "100Pals" (Steam) - Invitations only. Mention YouGotHitByGunner. Limited to 20 people. Note: At least 1000 trophies needed.

**[Warframe](https://store.steampowered.com/app/230410/Warframe/)** - "100Pals" (Steam) - Mention Allalinor, Hrulfyr or Hikikomori in #warframe-pals to join.

[icon-cme]: /images/content/cme_64x64.png "Completionist.me"
[icon-mgs]: /images/content/mgs_64x64.png "MetaGamerScore"
[icon-sh]: /images/content/sh_64x64.png "SteamHunters"
[icon-exo]: /images/content/exo_64x64.png "EXOPhase"
[icon-speedrun]: /images/content/speedrun_64x64.png "speedrun.com"
[icon-cur-vr]: /images/content/icon-cur-vr.gif
[icon-cur-scout]: /images/content/icon-cur-scout.gif
[icon-cur-pals]: /images/content/icon-cur-pals.gif
