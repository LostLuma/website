---
layout: default
title: How to Savescum
permalink: /guides/savescum
---

# What is savescumming?

Savescumming, as per the [rules](/rules#ok-4), is backing up your own save game, doing actions, then restoring it. This is useful for many achievements, such as those that require buying all the items in a game.

Using this technique, you can farm up the money you need, buy the necessary items - then restore your save game to have the achievement, no items, and all the money for use for what you ACTUALLY want in game.

# Downloading GameSave Manager

First of all, lets save a LOT of effort finding where savegames are located, and instead use a tool that already has a large database of savegames: [GameSave Manager](https://www.gamesave-manager.com/).

Just click the big download button as seen below.

![](/images/guides/savescum/download.png)

Once its downloaded - unzip it somewhere convinient

# Configure a backup folder

After launching the program, it will download updates, and ask you to configure a backup directory. If you need to do it manually - say you want to back up to an external hard drive, first click the "Program Settings" link.

![](/images/guides/savescum/config_1.png)

Then under `Task Specific -> Backup Settings`, select the hilighted options. This allows you to have multiple savescums per game.

![](/images/guides/savescum/config_2.png)

# Back up your games

Going back to the main menu, backing up games is similarly simple. 

Simply select `Make a Backup`.

![](/images/guides/savescum/backup_1.png)

Then, choose your game(s) to back up, and hit the button to start the process.

![](/images/guides/savescum/backup_2.png)

# Restore your games

Restoring games is equally simple. Starting at the main menu again:

Simply select `Open Archive(s)`.

![](/images/guides/savescum/restore_1.png)

Navigate to your backup folder, and select the backed up save that you wish to restore. Then click the `Open` button.

![](/images/guides/savescum/restore_2.png)

Finally, you'll end up on a screen very similar to the backup screen. Select the game(s) you wish you restore, then click the restore button.

![](/images/guides/savescum/restore_3.png)

Congratulations! You're now good to play your old save!
