---
layout: default
title: How to set up GameAssistant for Achievement Hunters
permalink: /guides/gameassistant
---

# How to set up GameAssistant for Achievement Hunters

[GameAssistant](https://store.steampowered.com/app/1190750/GameAssistant_The_Tool_For_Every_Gamer/) is a multi-game tweaking tool, that helps get around various issues. The developer has worked with 100Pals staff to create a configuration to ensure that these tweaks can be used without falling afoul of the [unified rules](/rules).

# The simple configuration

Its actually extremely easy to set up. Just open the config menu as shown here:

![](/images/guides/gameassistant/1.png)

Then just check all the achievement hunting boxes in the menu and hit save.

![](/images/guides/gameassistant/2.png)

Thats it! You're good to tweak your games and get playing! Please support the developer, for all their help making their lovely tool integrate so nicely with our community.
