---
layout: default
title: Relocking Guide
permalink: /guides/relock
redirects:
  - /relock
---

# How to relock your achievements

IMPORTANT: Do NOT do any of these steps without instructions from an [appeal on our forum!](https://forum.achievementhunting.com)

## Step 1: Download SAR (Windows Only)

You will need to start off by downloading the latest release of SAR from here: <https://github.com/Anaximand/SteamAchievementRelocker/releases>

![](/images/guides/relock/github.png)

Download and unzip all the files within to a folder of your choice

NOTE: If you are a linux or macos gamer, you may need to invalidate games or use WINE/Windows VM/Another computer/etc to use SAR, as there currently isn't a multiplatform application for this.

## Step 2: Lock achievements and clear stats with SAR

Start off by running SAR.Picker. This will load up a list of all your steam games. Scroll down to pick the game you need to reset.

![](/images/guides/relock/sar1.png)

Double click on the game, and it will open a window.

![](/images/guides/relock/sar2.png)

If you have been instructed to reset the game fully, you will need to click the Reset button on the menu bar, followed by Commit Changes

![](/images/guides/relock/sar3.png)

If you are being more specific regarding your locking, you will want to uncheck any achievement that needs to be reset.

![](/images/guides/relock/sar4.png)

For many games, you will also need to click the "Statistics" tab, and edit the statistics as well. As far as we've seen, the only default value for stats is "0"

![](/images/guides/relock/sar5.png)

## Step 3: Clearing Savegames

Usually, you will also need to clear your related savegames, as games will often re-pop achievements when your savegame is loaded.

For this, we recommend checking [PC Gaming Wiki](https://pcgamingwiki.com) to find the save game location. For the example above of 100% Orange Juice, lets check the related wiki page:

![](/images/guides/relock/pcwiki.png)

Here we can see the save location is the file `user.dat` in the game install folder, and the game has steam cloud support.

So, to remove the save game, a few steps need to happen. First off, open the properties for this game, and disable the steam cloud saving feature for it.

![](/images/guides/relock/steam1.png)

Under the `Updates` tab, uncheck the `Enable Steam Cloud synchronization for 100% Orange Juice` checkbox, and then hit Close.

![](/images/guides/relock/steam2.png)

Then, go to the location of the savegames, and delete the save file(s). NOTE: this may also involve editing the registry, depending on the game.

Launch the game, and ensure your relocked achievements do not re-unlock.

If it doesn't, then start a new game, and create a new save. You can then turn steam cloud saving back on, at which point on the next game launch, steam will say your games are desynced. Choose to overwrite the steam cloud with what's on your local computer, and you're done!

If you do get an achievement re-pop, then please leave a message on your appeal indicating this, so further instructions can be provided.

## Eratta: Known Oddities

TODO: Games with known odd issues / impossible relocks / etc will be listed here.
