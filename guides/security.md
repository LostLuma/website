---
layout: default
title: Security
permalink: /guides/security
redirects:
  - /security
---

# Securing Your Steam Account

Heya Pals - jippen here (I work in security). We’ve been seeing a lot of Steam accounts and the like being hacked, and I wanna help you all to not have to post things like this:

![](/images/guides/security/security1.png)

Or have your account start spamming all your Steam friends with things like this:

![](/images/guides/security/security2.png)

Or lose all those lovely games and all your glorious cheevos.

## How do Steam accounts get hacked?

There are 4 common ways for your Steam account to be compromised. We’ll go through them in descending order of likelihood:

### Shared/Same Passwords

This comes in two flavors: giving your password to friends/family members (which is a bad idea and against the steam ToS), and sharing your same password between different websites.

So, say you use the same password on Steam and SomeGamingForum.com. Now, a hacker gets into the much weaker security system of SomeGamingForum.com and steals all the email addresses and passwords. That hacker is likely gonna try re-using them on more interesting and valuable websites - like Facebook, eBay, and Steam.

![](/images/guides/security/security3.png)

### Phishing Page

This is a very common technique used by criminal hackers to steal accounts for a variety of services. As you can see below, this page looks like steamcommunity.com, but it’s actually going to steamcommnurty.com - an easy typo to miss. Type in your username and password and they’re sent off to hackers who will then login to your account and do bad things.

Some of the more advanced phishing pages will be fully automated, and will even ask for your Multi-Factor Authentication (MFA) Token, so be extra careful with them! If you use a password manager like LastPass or OnePassword, it will help you detect phishing pages as your credentials won’t be auto-filled in by your browser.

When in doubt, type in “<steamcommunity.com>” rather than trusting links, especially if that link is offering you something tempting, like csgo keys, free games, etc.

![](/images/guides/security/security4.png)

### Infected Computer

Welcome to the year 20XX, malware is still a thing. Not updating your computer, not running Antivirus software, and downloading and running random programs from the internet can all lead to your keystrokes being logged, passwords stolen, and having your account hacked and emptied.

### Weak Passwords

Most people are bad at passwords. Hackers know this, and will get lists of email addresses, and just try bad passwords all day and night. And if they get in - jackpot! Note the minimal changes in the chart below of the most common passwords after nine years of the news saying "USE BETTER PASSWORDS, PEOPLE".

**Most common passwords**

|   2009    |    2018   |
| --------- | --------- |
| 123456    | 123456    |
| 12345     | password  |
| 123456789 | 12345678  |
| password  | qwerty    |
| iloveyou  | 12345     |
| princess  | 123456789 |
| 1234567   | letmein   |
| rockyou   | 1234567   |
| 12345678  | football  |
| abc123    | iloveyou  |

## How do I prevent my account from getting hacked?

### Use a Password Manager

How do you stop the password re-use issue? By having a unique password for every site, of course. However, nobody has the brain space to remember a unique password for every website they use AND make them not suck.

So I recommend something different: encrypting password managers. There’s several great ones out there:

* [Keepass](https://keepass.info/) (Windows) or [KeepassXC](https://www.keepassxc.org/) (multi-platform) (Free)
* * optional [Keepass2Android](https://play.google.com/store/apps/details?id=keepass2android.keepass2android) (Android)
* [Enpass](https://www.enpass.io/) (Free, but with paid extras)
* [1Password](https://1password.com/) (Paid)
* [LastPass](https://www.lastpass.com/) (Free, but with paid extras)

They all work just great. One thing to note is Keepass is free BUT requires you to manage your own backups. The others will backup your password database as part of their service. Regardless of which you choose, the following steps should be taken:

1. Set your master password to a memorable sentence
2. Every time you visit a site you log into, change the password before saving it in your password manager
3. Use the password manager’s built in tool in order to generate random passwords - stop making your own!

### Setup Steam Guard

Multi-Factor Authentication (MFA) - sometimes called Two-Factor authentication (2FA) - is a way to keep your account safe even if your password is compromised. It’s a very good idea to help protect the value of your Steam account - which can be a lot higher than you might expect if you’ve been collecting games for a while.

![](/images/guides/security/security5.png)

Yikes. That’d suck to lose. Find more instructions on how to protect your Steam account here: <https://support.steampowered.com/kb_article.php?ref=4440-RTUI-9218>

### Keep Your Computer & Virus Scanner Up-To-Date

Most of the time, if your computer is infected with something, it's because you didn’t update your computer regularly. The most important things to keep updated are your Operating System (OS), Browser, Java, and Adobe Flash - but all software can be hacked, and updating helps keep you safe.

Here’s how you update your Windows OS: <https://support.microsoft.com/en-us/help/12373/windows-update-faq>

If you’re hunting on MacOS, I got you over here: <https://support.apple.com/en-us/HT201541>

For Anti-malware, my go-to has always been Malwarebytes. The free version is fantastic for spot scans, and the full version is both inexpensive and offers excellent real-time defense. Plus if you’re protecting multiple computers, Malwarebytes gives you a discount to protect them all.

As far as antivirus goes, it is quite hard to give a recommendation list that lasts the test of time as various vendors will ebb and flow as companies change direction. As of early 2019, these are all great choices:

* [ESET NOD32 Antivirus](https://www.eset.com/us/home/antivirus/)
* [Webroot AntiVirus](https://www.webroot.com/us/en/home)
* [Kaspersky Antivirus](https://usa.kaspersky.com/antivirus)
* [AVG Anti-virus](https://www.avg.com/en-us/free-antivirus-download) (Free, but basic)
* [Avira](https://www.avira.com/en/free-antivirus-windows) (Free, also basic)

And, after installing updates, remember to reboot every once in a while to ensure things get updated properly. But the basic rule is if your computer is asking you to restart Windows/Chrome/whatever for updates… just do it. It is better than getting hacked.

## Too late, I got hacked. How do I get my stuff back?

If you still have access to your account, change the password immediately and put an authenticator on it - this should boot out the spammers.

Then run an antivirus/antimalware scan. [Malwarebytes](https://www.malwarebytes.com/) is a good choice here, and has a free version that will help you clean up. Then follow up with the rest of the advice in the previous section to prevent this from happening again.

If you lost access to the account, Steam has an account recovery procedure available here: <https://help.steampowered.com/en/wizard/HelpWithAccountStolen> - this is likely going to be a long painful process, and Valve employees are the only ones with the access and ability to recover your account. Running around the internet and complaining may make you feel better, but a support ticket is your only opportunity to recover your account.
