100Pals Website
===============

This is a quick walkthrough on editing the 100Pals website

Tech Stack
----------

This site is built using [Jekyll](https://jekyllrb.com) using [Gitlab Pages](https://about.gitlab.com/product/pages/) for hosting behind a [CloudFlare](https://www.cloudflare.com/) CDN for high availability.

Upon commit to the master branch of the repo, all markdown/sass/etc files will be compiled into a static site and deployed. However, CloudFlare DOES cache these pages for 4 hours by default, so changes may take a while to appear globally.

Where are things?
-----------------

To edit the site content, you will want to focus on the appropriate .md files. They are all named after the URLs, so <https://achievementhunting.com/glossary> is managed by `glossary.md`

Exception: The unified rules is stored in the <https://gitlab.com/100pals/unified_hunting_rules> repo, so that the rules can be kept separate from the main site, and so that we can make PRs on the rules a separate public discussion, and keep it separate from the 100pals marketing page. Additionally, when the master branch is updated on the unified hunting rules repo, this site is built and re-deployed to keep everything in sync.

Images are all stored in `/images`, and the CSS is built using [SASS](https://sass-lang.com/) for cleaner implementation, which is sored in the `css/` and `_sass/` directories

The site template is handled by files in `_includes/` and `_layouts/`, and are pretty simple. Most of the heavy lifting is done via the CSS and the markdown conversion powered by Jekyll.

Editing the servers and staff pages are a little special. In order to get them to format nicely, I had to get pretty advanced with the HTML generation. They are generated from data stored in `_data/`, and when those files are edited the page will regenerate.

Behind the scenes
-----------------

The site is built by the `.gitlab-ci.yaml` file, which calls `.build.sh` to do the heavy lifting of calling Jekyll and syncing the rules page.

Gitlab CI/CD uses the `Gemfile` and `Gemfile.lock` files to bootstrap Jekyll

Jeykll, in turn, uses `_config.yml` to determine what needs to be done in order to build this site, and also what files to ignore from the finished site.

Editing the site
----------------

The preferred way to edit the site would be via a standard [gitlab pull request](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) so it can be reviewed by others before deploying. As the deployment process here is simple (janky), there's currently no separation between any sort of 'dev' and 'production' environment, so when master is updated, its go time.

If something needs to be updated right now, and the cache needs to be force cleared, contact Jippen. Eventually cache-clears will be part of the deployment process
